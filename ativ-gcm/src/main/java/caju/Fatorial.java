package caju;

public class Fatorial {

	public static int Fatorial(int n) {

		int fatorial = n;

		for (int i = 1; i < n; i++) {
			fatorial *= i;
		}

		return fatorial;
	}
}
