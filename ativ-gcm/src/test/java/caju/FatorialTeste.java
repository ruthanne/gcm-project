package caju;
import org.junit.Assert;
import org.junit.Test;

public class FatorialTeste {

	@Test
	public void fat() {
		int n = 8;
		int esperado = 40320;
		int result = Fatorial.Fatorial(n);

		Assert.assertEquals(esperado, result);
	}
}
